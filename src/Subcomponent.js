import React, { Component } from 'react'


const Test = ()=><h1>Hello World</h1>

export default class Subcomponent extends Component {
render() {
    console.log('Render')
    return (
      <div>
        <Test/>
        <h1>Number is: {this.props.num}</h1>
      </div>
    )
  }
}
