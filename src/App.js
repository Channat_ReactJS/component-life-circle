import React, { Component } from 'react';
import './App.css';
import {Button} from 'react-materialize'
import Subcomponent from './Subcomponent'
class App extends Component {
  constructor(){
    super()
    this.state={
      show:false,
      num:0
    }
  }
  render() {
    return (
      <div className="App">
        <Button onClick={()=>this.setState({show:!this.state.show})} >{this.state.show?'HIDE':'SHOW'}</Button>
        {this.state.show && <Button onClick={()=>this.setState({num:this.state.num+1})}>Add Num</Button>}
        {this.state.show && (<Subcomponent num={this.state.num}/>)}

      </div>
    );
  }
}

export default App;
